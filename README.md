# [cemu-web.gitlab.io](https://cemu-web.gitlab.io)

TI-84 Plus CE and TI-83 Premium CE emulator

Backend from [https://github.com/CE-Programming/CEmu](https://github.com/CE-Programming/CEmu)


[![pipeline status](https://gitlab.com/cemu-web/cemu-web.gitlab.io/badges/main/pipeline.svg)](https://gitlab.com/cemu-web/cemu-web.gitlab.io/-/commits/main) 

## Install

- Visit the link above
- Open the share sheet on your device
- Click `Add to Home Screen`

## Usage

- Open the emulator menu by clicking on the screen 
- Import a ROM image using the `Upload a ROM` button
    - To upload a different ROM, click `Reset All Data` and then click `Upload a ROM` again
- Upload files to the calculator using the `Upload Vars` button
- Reset the calculator's RAM using the `Reset RAM` button
- Change the background color using the color icons in the settings menu
- Change the speed and frame skip values to change the tick speed of the calculator and the rate at which frames get shown
